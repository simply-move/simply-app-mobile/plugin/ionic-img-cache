(function(document) {
  'use strict';

  angular
    .module('ionicImgCache', ['ionic'])
    .run(init)
    .provider('ionicImgCache', ionicImgCacheProvider)
    .factory('ionImgCacheSrv', ionImgCacheSrv)
    .directive('ionImgCache', ionImgCache)
    .directive('ionImgCacheBg', ionImgCacheBg);

  function init($ionicPlatform, ionicImgCache, $log, $document, $timeout) {
    /* ngInject */

    ImgCache.options.skipURIencoding = true;
    ImgCache.options.debug = ionicImgCache.debug;
    ImgCache.options.localCacheFolder = ionicImgCache.folder;
    ImgCache.options.chromeQuota = ionicImgCache.quota * 1024 * 1024;
    ImgCache.$log = $log;
    ImgCache.$timeout = $timeout;


    $ionicPlatform.ready(function() {
      ImgCache.init(function() {
        var message = 'ionicImgCache initialized';

        if (ionicImgCache.debug) {
          if ($log.info) {
            $log.info(message);
          }
          else {
            $log.log(message);
          }
        }
      }, function() {
        var message = 'Failed to init ionicImgCache.';

        if (ionicImgCache.debug) {
          if ($log.error) {
            $log.error(message);
          }
          else {
            $log.log(message);
          }
        }
      });
    });
  }

  function ionicImgCacheProvider() {
    var debug = false;
    var quota = 50;
    var folder = 'ionic-img-cache';

    this.debug = function(value) {
      debug = !!value;
    }

    this.quota = function(value) {
      quota = isFinite(value) ? value : 50;
    }

    this.folder = function(value) {
      folder = '' + value;
    }

    this.$get = function() {
      return {
        debug: debug,
        quota: quota,
        folder: folder
      };
    };
  }

  function ionImgCacheSrv($q) {
    /* ngInject */

    return {
      checkCacheStatus: checkCacheStatus,
      checkBgCacheStatus: checkBgCacheStatus,
      clearCache: clearCache,
      fillBackgroung: fillBackgroung,
      changeFillSvgColor: changeFillSvgColor
    };

    function checkCacheStatus(src) {
      var defer = $q.defer();

      _checkImgCacheReady()
        .then(function() {
          ImgCache.isCached(src, function(path, success) {
            if (success) {
              defer.resolve(path);
            } else {
              ImgCache.cacheFile(src, function() {
                ImgCache.isCached(src, function(path, success) {
                  defer.resolve(path);
                }, defer.reject);
              }, defer.reject);
            }
          }, defer.reject);
        })
        .catch(defer.reject);

      return defer.promise;
    }

    function checkBgCacheStatus(element) {
      var defer = $q.defer();

      _checkImgCacheReady()
        .then(function() {
          ImgCache.isBackgroundCached(element, function(path, success) {
            if (success) {
              defer.resolve(path);
            } else {
              ImgCache.cacheBackground(element, function() {
                ImgCache.isBackgroundCached(element, function(path, success) {
                  defer.resolve(path);
                }, defer.reject);
              }, defer.reject);
            }
          }, defer.reject);
        })
        .catch(defer.reject);

      return defer.promise;
    }

    function clearCache(element) {
      var defer = $q.defer();

      _checkImgCacheReady()
        .then(function() {
          if (angular.isUndefined(element)){
            ImgCache.clearCache(defer.resolve, defer.reject);
          }else{
            ImgCache.removeFile(element);
          }
        })
        .catch(defer.reject);

      return defer.promise;
    }

    function _checkImgCacheReady() {
      var defer = $q.defer();

      if (ImgCache.ready) {
        defer.resolve();
      }
      else{
        $document.addEventListener('ImgCacheReady', function() {
          defer.resolve();
        }, false);
      }

      return defer.promise;
    }
  }

  function getUriFileExt(uri){
    return /[^.]+$/.exec(uri);
  }

  function changeFillSvgColor(svg,color){
    var res = svg.replace(/fill=\"([^\"]*)\"/g,'fill="'+color+'"');
    res = res.replace(/\r?\n|\r/g, " ");
    res = res.replace(/\"/g, "\\\"");
    return res;
  }


  function fillBackgroung(context,element,newColor){
    ImgCache.$log.info('fillBackgroung');
    if (!ImgCache.private.isImgCacheLoaded()) {
            return;
    }

    var img_src = ImgCache.private.getBackgroundImageURL(element);
    if (!img_src) {
        ImgCache.$log.info('No background to cache');
        if (error_callback) { error_callback(); }
        return;
    }
    ImgCache.$log.info('File uri cached: '+ img_src);
    if (getUriFileExt(img_src) == "svg"){
      ImgCache.$log.info('Svg detected --> coloring');

       var _gotFileEntry = function (entry) {
            
               var _win = function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                       
                        var fileContent = e.target.result;
                        if (!fileContent) {
                            ImgCache.overridables.log('File in cache ' + filename + ' is empty', LOG_LEVEL_WARNING);
                            if (error_callback) { error_callback(element); }
                            return;
                        }
                        
                        var _newSvg = context.changeFillSvgColor(fileContent,newColor);

                        //element.css('background', "url('data:image/svg+xml;utf8,"+fileContent+"')");
                        element.css('background', "url('data:image/svg+xml;utf8,"+_newSvg+"')");
                        //if (success_callback) { success_callback(element); }
                    };
                    reader.readAsText(file);
                };
                var _fail = function (error) {
                    ImgCache.overridables.log('Failed to read file ' + error.code, LOG_LEVEL_ERROR);
                    //if (error_callback) { error_callback($element); }
                };

                entry.file(_win, _fail);
            
        };
        // if file does not exist in cache, cache it now!
        var _fail = function () {
            ImgCache.overridables.log('File ' + filename + ' not in cache', LOG_LEVEL_INFO);
            if (error_callback) { error_callback(element); }
        };



      ImgCache.attributes.filesystem.root.getFile(ImgCache.private.getCachedFilePath(img_src), {create: false}, _gotFileEntry, _fail);
    }else{
      ImgCache.private.loadCachedFile(element, img_src, ImgCache.private.setBackgroundImagePath, success_callback, error_callback);
    }
  }

  function ionImgCache(ionImgCacheSrv) {
    /* ngInject */

    return {
      restrict: 'A',
      link: link
    };

    function link(scope, element, attrs) {
      attrs.$observe('ngSrc', function(src) {
        if (!ionic.Platform.isWebView()) return;
        ionImgCacheSrv.checkCacheStatus(src)
          .then(function() {
            ImgCache.useCachedFile(element);
          });
      });
    }
  }

  function ionImgCacheBg(ionImgCacheSrv) {
    /* ngInject */

    return {
      restrict: 'A',
      link: link
    };

    function link(scope, element, attrs) {
      ImgCache.overridables.log('svg color: ' + attrs.svgColor, LOG_LEVEL_INFO);
      ionImgCacheSrv.checkBgCacheStatus(element)
        .then(function() {
          if (!ionic.Platform.isWebView()) return;
            if (attrs.svgColor){
              ionImgCacheSrv.fillBackgroung(ionImgCacheSrv,element,attrs.svgColor);
            }else{
              ImgCache.useCachedBackground(element);
            }
        });

      attrs.$observe('ngStyle', function() {
        ionImgCacheSrv.checkBgCacheStatus(element)
          .then(function() {
            if (!ionic.Platform.isWebView()) return;
              if (attrs.svgColor){
                ionImgCacheSrv.fillBackgroung(ionImgCacheSrv,element,attrs.svgColor);
              }else{
                ImgCache.useCachedBackground(element);
              }
          });
      });
    }
  }
})(document);


